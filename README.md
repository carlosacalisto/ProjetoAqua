# projeto inicial

## Hierarquia
- **/app** Diretório da aplicação

## Porta
- **A porta padrão está definida como 3001**

## Uso
Na pasta /app, execute o comando para instalar as dependências:

```bash
npm install
```

Inicie o servidor com o comando:
```bash
npm start
```
---
 