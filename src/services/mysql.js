const mysql = require('mysql');

var pool = mysql.createPool({
    "connectionLimit" : 1000,
    "user" : "root",
    "password": "",
    "database" : "ai_aquario",
    "host": "localhost",
    "port" : 3306
});

pool.getConnection(function(err, connection) {
    if (err){
        console.log('erro ao conectar o banco ')
        console.log(err)
    }else{
        console.log('Banco conectado com sucesso')

    }  
});
 
exports.execute = (query, params=[]) => {
    return new Promise((resolve, reject) => {
        pool.query(query, params, (error, result, fields) => {

            //console.log(error)
            if (error) {
                reject(error);
            } else {
                resolve(result)
                
            }
           pool.on('release', function (connection) {
                //console.log('Connection %d released', connection.threadId);
              });  
            
        });
    })
}




exports.pool = pool;