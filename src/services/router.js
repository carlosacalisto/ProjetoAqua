const express = require('express')
const router = new express.Router()
//const status = require('../controllers/status')

const tipoSensor = require('../controllers/tipoSensor')
const local = require('../controllers/Local')
const LogBruto = require('../controllers/LogBruto')
const Periferico = require('../controllers/Periferico')

//Periferico
router.route('/getPeriferico').get(Periferico.getListar)
router.route('/getBuscaPerifericoPorCodigo/:CODIGO').get(Periferico.getRegistroPorCodigo)
router.route('/postRegistroPeriferico').post(Periferico.postRegistro)
router.route('/putRegistroPeriferico').put(Periferico.putRegistro)
router.route('/delRegistroPeriferico/:CODIGO').delete(Periferico.delRegistro)

//Sensor
router.route('/getTipoSensor').get(tipoSensor.getListar)
router.route('/getBuscaTipoSensorPorCodigo/:CODIGO').get(tipoSensor.getRegistroPorCodigo)
router.route('/postRegistrotipoSensor').post(tipoSensor.postRegistro)
router.route('/putRegistrotipoSensor').put(tipoSensor.putRegistro)
router.route('/delRegistrotipoSensor/:CODIGO').delete(tipoSensor.delRegistro)

//Local
router.route('/getlocal').get(local.getListar)
router.route('/getBuscalocalPorCodigo/:CODIGO').get(local.getRegistroPorCodigo)
router.route('/postRegistrolocal').post(local.postRegistro)
router.route('/putRegistrolocal').put(local.putRegistro)
router.route('/delRegistrolocal/:CODIGO').delete(local.delRegistro)


//AÇÃO DO ESP32 - LEMBRANDO QUE TEM QUE PASSAR
//LogBruto
router.route('/InserirLogBruto').post(LogBruto.InserirLog)
router.route('/getAtualizaSensoresESP/:COD_LOCAL').get(Periferico.getAtualizaSensoresESP)
 

 


module.exports = router