const mysql = require('../services/mysql.js');

async function InserirLogAcao_db(context) {
    const bind = {}
    //console.log(context)  , 
    bind.COD_PERIFERICO = context.COD_PERIFERICO
    bind.ESTADO_NOVO = context.ESTADO_NOVO
    bind.ESTADO_ANTIGO = context.ESTADO_ANTIGO
    bind.COD_USUARIO = context.COD_USUARIO
    let query = `INSERT INTO log_acao (COD_PERIFERICO, ESTADO_NOVO, ESTADO_ANTIGO, DATA, COD_USUARIO) VALUES 
    ('${bind.COD_PERIFERICO}','${bind.ESTADO_NOVO}','${bind.ESTADO_ANTIGO}', NOW() ,'${bind.COD_USUARIO}')`

    //console.log(query)
    try {
        const result = await mysql.execute(query, bind)
        return result
    } catch (err) {
        return err
    }
}
module.exports.InserirLogAcao_db = InserirLogAcao_db
 
  