const mysql = require('../services/mysql.js');

async function getListar_db(context) {
    let query = `SELECT * FROM tipo_sensor`
    const bind = {}
    try {
        const result = await mysql.execute(query, bind)
        console.log(result)
        return result
    } catch (err) {
        return err
    }
}
module.exports.getListar_db = getListar_db


async function getRegistroPorCodigo_db(context) {
    const bind = {}
    bind.CODIGO = context.CODIGO
    let query = `SELECT * FROM tipo_sensor WHERE CODIGO = ${bind.CODIGO} `

    try {
        const result = await mysql.execute(query, bind)
        //console.log(result)
        return result
    } catch (err) {
        return err
    }
}
module.exports.getRegistroPorCodigo_db = getRegistroPorCodigo_db


async function postRegistro_db(context) {
    const bind = {}
    bind.NOME = context.NOME
    bind.UNIDADE_MEDIDA = context.UNIDADE_MEDIDA
    bind.TIPO_SENSOR = context.TIPO_SENSOR

    console.log(bind)


    let query = `INSERT INTO tipo_sensor (NOME, UNIDADE_MEDIDA, TIPO_SENSOR) 
        VALUES 
        ('${bind.NOME}',  '${bind.UNIDADE_MEDIDA}',  '${bind.TIPO_SENSOR}'); 
        `

    try {
        const result = await mysql.execute(query, bind)
        //console.log(result)
        return result
    } catch (err) {
        return err
    }
}
module.exports.postRegistro_db = postRegistro_db




async function putRegistro_db(context) {
    const bind = {}
    bind.NOME = context.NOME
    bind.UNIDADE_MEDIDA = context.UNIDADE_MEDIDA
    bind.TIPO_SENSOR = context.TIPO_SENSOR
    bind.CODIGO = context.CODIGO
    let query = `UPDATE tipo_sensor SET 
        nome='${bind.NOME}',
        unidade_medida='${bind.UNIDADE_MEDIDA}',
        tipo_sensor='${bind.TIPO_SENSOR}'
         WHERE codigo ='${bind.CODIGO}';
        `

    try {
        const result = await mysql.execute(query, bind)
        //console.log(result)
        return result
    } catch (err) {
        return err
    }
}
module.exports.putRegistro_db = putRegistro_db


async function delRegistro_db(context) {
    const bind = {}
    bind.CODIGO = context.CODIGO

    let query = `DELETE FROM tipo_sensor WHERE codigo = '${bind.CODIGO}' ; `


    try {
        const result = await mysql.execute(query, bind)
        //console.log(result)
        return result
    } catch (err) {
        return err
    }
}
module.exports.delRegistro_db = delRegistro_db

