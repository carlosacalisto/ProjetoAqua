const mysql = require('../services/mysql.js');

async function getListar_db(context) {
    let query = `SELECT * FROM periferico`
    const bind = {}
    try {
        const result = await mysql.execute(query, bind)
        console.log(result)
        return result
    } catch (err) {
        return err
    }
}
module.exports.getListar_db = getListar_db


async function getRegistroPorCodigo_db(context) {
    const bind = {}
    bind.CODIGO = context.CODIGO
    let query = `SELECT * FROM periferico WHERE CODIGO = ${bind.CODIGO} `

    try {
        const result = await mysql.execute(query, bind)
        //console.log(result)
        return result
    } catch (err) {
        return err
    }
}
module.exports.getRegistroPorCodigo_db = getRegistroPorCodigo_db


async function postRegistro_db(context) {
    const bind = {}
    bind.TAG = context.TAG
    bind.COD_LOCAL = context.COD_LOCAL
    bind.COD_TIPO = context.COD_TIPO
    bind.ESTADO = context.ESTADO
    bind.PINO_ENTRADA = context.PINO_ENTRADA
    bind.DESCRICAO = context.DESCRICAO
 

    let query = `INSERT INTO periferico (TAG, COD_LOCAL, COD_TIPO, ESTADO, PINO_ENTRADA, DESCRICAO ) 
        VALUES 
        ('${bind.TAG}',  '${bind.COD_LOCAL}',  '${bind.COD_TIPO}',  '${bind.ESTADO}',  '${bind.PINO_ENTRADA}',  '${bind.DESCRICAO}'); 
        `

    try {
        const result = await mysql.execute(query, bind)
        //console.log(result)
        return result
    } catch (err) {
        return err
    }
}
module.exports.postRegistro_db = postRegistro_db




async function putRegistro_db(context) {
    const bind = {}
    bind.TAG = context.TAG
    bind.COD_LOCAL = context.COD_LOCAL
    bind.COD_TIPO = context.COD_TIPO
    bind.ESTADO = context.ESTADO
    bind.PINO_ENTRADA = context.PINO_ENTRADA
    bind.DESCRICAO = context.DESCRICAO
    bind.CODIGO = context.CODIGO

    let query = `UPDATE periferico SET 
    TAG='${bind.TAG}',
    COD_LOCAL='${bind.COD_LOCAL}',
    COD_TIPO='${bind.COD_TIPO}',
    ESTADO='${bind.ESTADO}',
    PINO_ENTRADA='${bind.PINO_ENTRADA}',
    DESCRICAO='${bind.DESCRICAO}' 
         WHERE codigo ='${bind.CODIGO}';
        `

    try {
        const result = await mysql.execute(query, bind)
        //console.log(result)
        return result
    } catch (err) {
        return err
    }
}
module.exports.putRegistro_db = putRegistro_db


async function delRegistro_db(context) {
    const bind = {}
    bind.CODIGO = context.CODIGO

    let query = `DELETE FROM periferico WHERE codigo = '${bind.CODIGO}' ; `


    try {
        const result = await mysql.execute(query, bind)
        //console.log(result)
        return result
    } catch (err) {
        return err
    }
}
module.exports.delRegistro_db = delRegistro_db



async function getBuscarCodigoPorTag_db(context) {
    const bind = {}
    bind.TAG = context.TAG
    let query = `SELECT CODIGO FROM periferico WHERE TAG = ${bind.TAG} `
    try {
        const result = await mysql.execute(query, bind)
        return result
    } catch (err) {
        return err
    }
}
module.exports.getBuscarCodigoPorTag_db = getBuscarCodigoPorTag_db



async function getAtualizaSensoresESP_db(context) {
    const bind = {}
    bind.COD_LOCAL = context.COD_LOCAL
    let query = `SELECT 
    (SELECT ESTADO FROM periferico WHERE cod_local = ${bind.COD_LOCAL} AND TAG = 'RELE1') AS RELE1, 
    (SELECT ESTADO FROM periferico WHERE cod_local = ${bind.COD_LOCAL} AND TAG = 'RELE2') AS RELE2,
    (SELECT ESTADO FROM periferico WHERE cod_local = ${bind.COD_LOCAL} AND TAG = 'RELE3') AS RELE3,
    (SELECT ESTADO FROM periferico WHERE cod_local = ${bind.COD_LOCAL} AND TAG = 'RELE4') AS RELE4,
    (SELECT ESTADO FROM periferico WHERE cod_local = ${bind.COD_LOCAL} AND TAG = 'RELE5') AS RELE5 
    FROM DUAL    
     `
 
    try {
        const result = await mysql.execute(query, bind)
        //console.log(result)
        return result
    } catch (err) {
        return err
    }
}
module.exports.getAtualizaSensoresESP_db = getAtualizaSensoresESP_db