const mysql = require('../services/mysql.js');

async function getListar_db(context) {
    let query = `SELECT * FROM local`
    const bind = {}
    try {
        const result = await mysql.execute(query, bind)
        console.log(result)
        return result
    } catch (err) {
        return err
    }
}
module.exports.getListar_db = getListar_db


async function getRegistroPorCodigo_db(context) {
    const bind = {}
    bind.CODIGO = context.CODIGO
    let query = `SELECT * FROM local WHERE CODIGO = ${bind.CODIGO} `

    try {
        const result = await mysql.execute(query, bind)
        //console.log(result)
        return result
    } catch (err) {
        return err
    }
}
module.exports.getRegistroPorCodigo_db = getRegistroPorCodigo_db


async function postRegistro_db(context) {
    const bind = {}
    bind.NOME = context.NOME
    bind.DESCRICAO = context.DESCRICAO
    bind.TAG_LOCAL = context.TAG_LOCAL
    console.log(bind)


    let query = `INSERT INTO local (NOME, DESCRICAO, TAG_LOCAL ) 
        VALUES 
        ('${bind.NOME}', '${bind.DESCRICAO}', '${bind.TAG_LOCAL}'); 
        `

    try {
        const result = await mysql.execute(query, bind)
        //console.log(result)
        return result
    } catch (err) {
        return err
    }
}
module.exports.postRegistro_db = postRegistro_db




async function putRegistro_db(context) {
    const bind = {}
    bind.NOME = context.NOME
    bind.DESCRICAO = context.DESCRICAO
    bind.CODIGO = context.CODIGO
    bind.TAG_LOCAL = context.TAG_LOCAL
    

    let query = `UPDATE local SET 
    NOME='${bind.NOME}',
    TAG_LOCAL='${bind.TAG_LOCAL}',
    DESCRICAO='${bind.DESCRICAO}' 
         WHERE codigo ='${bind.CODIGO}';
        `

    try {
        const result = await mysql.execute(query, bind)
        //console.log(result)
        return result
    } catch (err) {
        return err
    }
}
module.exports.putRegistro_db = putRegistro_db


async function delRegistro_db(context) {
    const bind = {}
    bind.CODIGO = context.CODIGO

    let query = `DELETE FROM local WHERE codigo = '${bind.CODIGO}' ; `


    try {
        const result = await mysql.execute(query, bind)
        //console.log(result)
        return result
    } catch (err) {
        return err
    }
}
module.exports.delRegistro_db = delRegistro_db

