const mysql = require('../services/mysql.js');


async function InserirLog_db(context) {
    const bind = {}
    //console.log(context)
    bind.COD_PERIFERICO = context.COD_PERIFERICO
    bind.VALOR = context.VALOR
    let query = `INSERT INTO log_bruto (COD_PERIFERICO, VALOR, DATA) VALUES ('${bind.COD_PERIFERICO}','${bind.VALOR}', NOW())`
    try {
        const result = await mysql.execute(query, bind)
        return result
    } catch (err) {
        return err
    }
}
module.exports.InserirLog_db = InserirLog_db
