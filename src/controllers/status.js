async function get(req, res, next) {
    try {
        res.status(200).send('ok')
    } catch (err) {
        next(err)
    }
}
module.exports.get = get
 