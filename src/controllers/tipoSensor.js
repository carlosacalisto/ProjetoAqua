const tipoSensor = require('../db_api/db_tipoSensor')
const validator = require('validator')

async function getListar(req, res, next) {
    try {
        const context = {}
        const rows = await tipoSensor.getListar_db(context)
        res.status(200).json(rows)
    } catch (err) {
        res.status(500).end()
    }
}
module.exports.getListar = getListar


async function getRegistroPorCodigo(req, res, next) {
    if (!validator.isNumeric(req.params.CODIGO + '')) {
        res.status(400).send({ message: 'CODIGO invalido' }).end()
        return null
    }
    if (!req.params.CODIGO) {
        res.status(400).send({ message: 'CODIGO não existe' }).end()
        return null
    }

    try {
        const context = {}

        context.CODIGO = req.params.CODIGO

        const rows = await tipoSensor.getRegistroPorCodigo_db(context)
        res.status(200).json(rows)
    } catch (err) {
        res.status(500).end()
    }
}
module.exports.getRegistroPorCodigo = getRegistroPorCodigo



async function postRegistro(req, res, next) {
    /* if (!validator.isNumeric(req.params.CODIGO + '')) {
         res.status(400).send({ message: 'CODIGO invalido' }).end()
         return null
     }
     if (!req.params.CODIGO) {
         res.status(400).send({ message: 'CODIGO não existe' }).end()
         return null
     }  */

    if (validator.isEmpty(req.body.NOME)) {
        res.status(400).send({ message: 'NOME invalido' }).end()
        return null
    }
    if (!req.body.NOME) {
        res.status(400).send({ message: 'NOME não existe' }).end()
        return null
    }

    if (validator.isEmpty(req.body.TIPO_SENSOR)) {
        res.status(400).send({ message: 'TIPO_SENSOR invalido' }).end()
        return null
    }
    if (!req.body.TIPO_SENSOR) {
        res.status(400).send({ message: 'TIPO_SENSOR não existe' }).end()
        return null
    }


    if (validator.isEmpty(req.body.UNIDADE_MEDIDA)) {
        res.status(400).send({ message: 'UNIDADE_MEDIDA invalido' }).end()
        return null
    }
    if (!req.body.UNIDADE_MEDIDA) {
        res.status(400).send({ message: 'UNIDADE_MEDIDA não existe' }).end()
        return null
    }

    try {
        const context = {}
        /* context.CODIGO = req.params.CODIGO */
        context.NOME = req.body.NOME
        context.UNIDADE_MEDIDA = req.body.UNIDADE_MEDIDA
        context.TIPO_SENSOR = req.body.TIPO_SENSOR


        const rows = await tipoSensor.postRegistro_db(context)
        res.status(200).json(rows)
    } catch (err) {
        res.status(500).end()
    }
}
module.exports.postRegistro = postRegistro


async function putRegistro(req, res, next) {
    if (!validator.isNumeric(req.body.CODIGO + '')) {
        res.status(400).send({ message: 'CODIGO invalido' }).end()
        return null
    }
    if (!req.body.CODIGO) {
        res.status(400).send({ message: 'CODIGO não existe' }).end()
        return null
    }

    if (validator.isEmpty(req.body.NOME)) {
        res.status(400).send({ message: 'NOME invalido' }).end()
        return null
    }
    if (!req.body.NOME) {
        res.status(400).send({ message: 'NOME não existe' }).end()
        return null
    }

    if (validator.isEmpty(req.body.TIPO_SENSOR)) {
        res.status(400).send({ message: 'TIPO_SENSOR invalido' }).end()
        return null
    }
    if (!req.body.TIPO_SENSOR) {
        res.status(400).send({ message: 'TIPO_SENSOR não existe' }).end()
        return null
    }


    if (validator.isEmpty(req.body.UNIDADE_MEDIDA)) {
        res.status(400).send({ message: 'UNIDADE_MEDIDA invalido' }).end()
        return null
    }
    if (!req.body.UNIDADE_MEDIDA) {
        res.status(400).send({ message: 'UNIDADE_MEDIDA não existe' }).end()
        return null
    }


    try {
        const context = {}
        context.CODIGO = req.body.CODIGO
        context.NOME = req.body.NOME
        context.UNIDADE_MEDIDA = req.body.UNIDADE_MEDIDA
        context.TIPO_SENSOR = req.body.TIPO_SENSOR
        const rows = await tipoSensor.putRegistro_db(context)
        res.status(200).json(rows)
    } catch (err) {
        res.status(500).end()
    }
}
module.exports.putRegistro = putRegistro



async function delRegistro(req, res, next) {
    if (!validator.isNumeric(req.params.CODIGO + '')) {
        res.status(400).send({ message: 'CODIGO invalido' }).end()
        return null
    }
    if (!req.params.CODIGO) {
        res.status(400).send({ message: 'CODIGO não existe' }).end()
        return null
    }

    try {
        const context = {}
        context.CODIGO = req.params.CODIGO
        const rows = await tipoSensor.delRegistro_db(context)
        res.status(200).json(rows)
    } catch (err) {
        res.status(500).end()
    }
}
module.exports.delRegistro = delRegistro




