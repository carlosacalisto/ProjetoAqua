const LogBruto = require('../db_api/db_LogBruto')
const periferico = require('./Periferico')
const validator = require('validator')

async function InserirLog(req, res, next) {
    const CODIGO_PERIFERICO = await periferico.getBuscarCodigoPorTag(req.body.TAG)
    if (!validator.isNumeric(CODIGO_PERIFERICO[0].CODIGO + '')) {
        res.status(405).send({ message: 'Não BUSCAR O CODIGO_PERIFERICO' }).end()
        return null
    }

    try {
        const context = {}
        context.COD_PERIFERICO = CODIGO_PERIFERICO[0].CODIGO
        context.VALOR = req.body.VALOR
        const rows = await LogBruto.InserirLog_db(context)
        res.status(200).json(rows)
    } catch (err) {
        res.status(500).end()
    }
}
module.exports.InserirLog = InserirLog

