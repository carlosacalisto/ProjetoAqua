const local = require('../db_api/db_local')
const validator = require('validator')

async function getListar(req, res, next) {
    try {
        const context = {}
        const rows = await local.getListar_db(context)
        res.status(200).json(rows)
    } catch (err) {
        res.status(500).end()
    }
}
module.exports.getListar = getListar


async function getRegistroPorCodigo(req, res, next) {
    if (!validator.isNumeric(req.params.CODIGO + '')) {
        res.status(400).send({ message: 'CODIGO invalido' }).end()
        return null
    }
    if (!req.params.CODIGO) {
        res.status(400).send({ message: 'CODIGO não existe' }).end()
        return null
    }

    try {
        const context = {}

        context.CODIGO = req.params.CODIGO

        const rows = await local.getRegistroPorCodigo_db(context)
        res.status(200).json(rows)
    } catch (err) {
        res.status(500).end()
    }
}
module.exports.getRegistroPorCodigo = getRegistroPorCodigo



async function postRegistro(req, res, next) {

    if (validator.isEmpty(req.body.NOME)) {
        res.status(400).send({ message: 'NOME invalido' }).end()
        return null
    }
    if (!req.body.NOME) {
        res.status(400).send({ message: 'NOME não existe' }).end()
        return null
    }
 
    if (validator.isEmpty(req.body.TAG_LOCAL)) {
        res.status(400).send({ message: 'TAG_LOCAL invalido' }).end()
        return null
    }
    if (!req.body.TAG_LOCAL) {
        res.status(400).send({ message: 'TAG_LOCAL não existe' }).end()
        return null
    }
    

    try {
        const context = {}
        context.NOME = req.body.NOME
        context.DESCRICAO = req.body.DESCRICAO
        context.TAG_LOCAL = req.body.TAG_LOCAL

        const rows = await local.postRegistro_db(context)
        res.status(200).json(rows)
    } catch (err) {
        res.status(500).end()
    }
}
module.exports.postRegistro = postRegistro


async function putRegistro(req, res, next) {
    if (!validator.isNumeric(req.body.CODIGO + '')) {
        res.status(400).send({ message: 'CODIGO invalido' }).end()
        return null
    }
    if (!req.body.CODIGO) {
        res.status(400).send({ message: 'CODIGO não existe' }).end()
        return null
    }

    if (validator.isEmpty(req.body.NOME)) {
        res.status(400).send({ message: 'NOME invalido' }).end()
        return null
    }
    if (!req.body.NOME) {
        res.status(400).send({ message: 'NOME não existe' }).end()
        return null
    }
  
    if (validator.isEmpty(req.body.TAG_LOCAL)) {
        res.status(400).send({ message: 'TAG_LOCAL invalido' }).end()
        return null
    }
    if (!req.body.TAG_LOCAL) {
        res.status(400).send({ message: 'TAG_LOCAL não existe' }).end()
        return null
    }
    

    try {
        const context = {}
        context.CODIGO = req.body.CODIGO
        context.NOME = req.body.NOME
        context.DESCRICAO = req.body.DESCRICAO
        context.TAG_LOCAL = req.body.TAG_LOCAL
        const rows = await local.putRegistro_db(context)
        res.status(200).json(rows)
    } catch (err) {
        res.status(500).end()
    }
}
module.exports.putRegistro = putRegistro



async function delRegistro(req, res, next) {
    if (!validator.isNumeric(req.params.CODIGO + '')) {
        res.status(400).send({ message: 'CODIGO invalido' }).end()
        return null
    }
    if (!req.params.CODIGO) {
        res.status(400).send({ message: 'CODIGO não existe' }).end()
        return null
    }

    try {
        const context = {}
        context.CODIGO = req.params.CODIGO
        const rows = await local.delRegistro_db(context)
        res.status(200).json(rows)
    } catch (err) {
        res.status(500).end()
    }
}
module.exports.delRegistro = delRegistro




 