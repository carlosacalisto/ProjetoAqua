const LogAcao = require('../db_api/db_LogAcao')
const validator = require('validator')

async function InserirLogAcao(COD_PERIFERICO, ESTADO_NOVO, ESTADO_ANTIGO) {
    try {
        const context = {}
        context.COD_PERIFERICO = COD_PERIFERICO
        context.ESTADO_NOVO = ESTADO_NOVO 
        context.ESTADO_ANTIGO = ESTADO_ANTIGO 
        context.COD_USUARIO = '00'

       // console.log(context)
        const rows = await LogAcao.InserirLogAcao_db(context)
         if(rows){
            return rows
         }else{
            console.log('erro 400')
            return null 
         }
    } catch (err) {
        console.log('erro 500')
    return err;
    }
}
module.exports.InserirLogAcao = InserirLogAcao

