const acao = require('../db_api/db_Acao')
const validator = require('validator')

async function getListar(req, res, next) {
    try {
        const context = {}
        const rows = await acao.getListar_db(context)
        res.status(200).json(rows)
    } catch (err) {
        res.status(500).end()
    }
}
module.exports.getListar = getListar


async function getPorCodigo(req, res, next) {
    if (!validator.isNumeric(req.params.CODIGO + '')) {
        res.status(400).send({ message: 'CODIGO invalido' }).end()
        return null
    }
    if (!req.params.CODIGO) {
        res.status(400).send({ message: 'CODIGO não existe' }).end()
        return null
    }

    try {
        const context = {}
        context.CODIGO = req.params.CODIGO
        const rows = await acao.getPorCodigo_db(context)
        res.status(200).json(rows)
    } catch (err) {
        res.status(500).end()
    }
}
module.exports.getPorCodigo = getPorCodigo