const Periferico = require('../db_api/db_periferico')
const validator = require('validator')
const logAcao = require('./logAcao')

async function getListar(req, res, next) {
    try {
        const context = {}
        const rows = await Periferico.getListar_db(context)
        res.status(200).json(rows)
    } catch (err) {
        res.status(500).end()
    }
}
module.exports.getListar = getListar


async function getRegistroPorCodigo(req, res, next) {
    if (!validator.isNumeric(req.params.CODIGO + '')) {
        res.status(400).send({ message: 'CODIGO invalido' }).end()
        return null
    }
    if (!req.params.CODIGO) {
        res.status(400).send({ message: 'CODIGO não existe' }).end()
        return null
    }

    try {
        const context = {}

        context.CODIGO = req.params.CODIGO

        const rows = await Periferico.getRegistroPorCodigo_db(context)
        res.status(200).json(rows)
    } catch (err) {
        res.status(500).end()
    }
}
module.exports.getRegistroPorCodigo = getRegistroPorCodigo



async function postRegistro(req, res, next) {

    if (validator.isEmpty(req.body.TAG)) {
        res.status(400).send({ message: 'TAG invalido' }).end()
        return null
    }
    if (!req.body.TAG) {
        res.status(400).send({ message: 'TAG não existe' }).end()
        return null
    }

    if (!validator.isNumeric(req.body.COD_LOCAL + '')) {
        res.status(400).send({ message: 'COD_LOCAL invalido' }).end()
        return null
    }
    if (!req.body.COD_LOCAL) {
        res.status(400).send({ message: 'COD_LOCAL não existe' }).end()
        return null
    }

    if (!validator.isNumeric(req.body.COD_TIPO + '')) {
        res.status(400).send({ message: 'COD_TIPO invalido' }).end()
        return null
    }
    if (!req.body.COD_TIPO) {
        res.status(400).send({ message: 'COD_TIPO não existe' }).end()
        return null
    }

    if (validator.isEmpty(req.body.ESTADO)) {
        res.status(400).send({ message: 'ESTADO invalido' }).end()
        return null
    }
    if (!req.body.ESTADO) {
        res.status(400).send({ message: 'ESTADO não existe' }).end()
        return null
    }

    if (validator.isEmpty(req.body.PINO_ENTRADA)) {
        res.status(400).send({ message: 'PINO_ENTRADA invalido' }).end()
        return null
    }
    if (!req.body.PINO_ENTRADA) {
        res.status(400).send({ message: 'PINO_ENTRADA não existe' }).end()
        return null
    }

    try {
        const context = {}
        /* context.CODIGO = req.params.CODIGO */
        context.TAG = req.body.TAG
        context.COD_LOCAL = req.body.COD_LOCAL
        context.COD_TIPO = req.body.COD_TIPO
        context.ESTADO = req.body.ESTADO
        context.PINO_ENTRADA = req.body.PINO_ENTRADA
        context.DESCRICAO = req.body.DESCRICAO

        const rows = await Periferico.postRegistro_db(context)

        const returno = await logAcao.InserirLogAcao(rows.insertId, context.ESTADO, ' ')
        //console.log(returno)


        res.status(200).json(rows)
    } catch (err) {
        res.status(500).end()
    }
}
module.exports.postRegistro = postRegistro


async function putRegistro(req, res, next) {
    if (!validator.isNumeric(req.body.CODIGO + '')) {
        res.status(400).send({ message: 'CODIGO invalido' }).end()
        return null
    }
    if (!req.body.CODIGO) {
        res.status(400).send({ message: 'CODIGO não existe' }).end()
        return null
    }

    if (validator.isEmpty(req.body.TAG)) {
        res.status(400).send({ message: 'TAG invalido' }).end()
        return null
    }
    if (!req.body.TAG) {
        res.status(400).send({ message: 'TAG não existe' }).end()
        return null
    }

    if (!validator.isNumeric(req.body.COD_LOCAL + '')) {
        res.status(400).send({ message: 'COD_LOCAL invalido' }).end()
        return null
    }
    if (!req.body.COD_LOCAL) {
        res.status(400).send({ message: 'COD_LOCAL não existe' }).end()
        return null
    }

    if (!validator.isNumeric(req.body.COD_TIPO + '')) {
        res.status(400).send({ message: 'COD_TIPO invalido' }).end()
        return null
    }
    if (!req.body.COD_TIPO) {
        res.status(400).send({ message: 'COD_TIPO não existe' }).end()
        return null
    }

    if (validator.isEmpty(req.body.ESTADO)) {
        res.status(400).send({ message: 'ESTADO invalido' }).end()
        return null
    }
    if (!req.body.ESTADO) {
        res.status(400).send({ message: 'ESTADO não existe' }).end()
        return null
    }

    if (validator.isEmpty(req.body.PINO_ENTRADA)) {
        res.status(400).send({ message: 'PINO_ENTRADA invalido' }).end()
        return null
    }
    if (!req.body.PINO_ENTRADA) {
        res.status(400).send({ message: 'PINO_ENTRADA não existe' }).end()
        return null
    }

    try {
        const context = {}
        context.CODIGO = req.body.CODIGO
        context.TAG = req.body.TAG
        context.COD_LOCAL = req.body.COD_LOCAL
        context.COD_TIPO = req.body.COD_TIPO
        context.ESTADO = req.body.ESTADO
        context.PINO_ENTRADA = req.body.PINO_ENTRADA
        context.DESCRICAO = req.body.DESCRICAO
        const rows = await Periferico.putRegistro_db(context)

        const returno = await logAcao.InserirLogAcao(context.CODIGO, context.ESTADO, ' ')

        res.status(200).json(rows)
    } catch (err) {
        res.status(500).end()
    }
}
module.exports.putRegistro = putRegistro



async function delRegistro(req, res, next) {
    if (!validator.isNumeric(req.params.CODIGO + '')) {
        res.status(400).send({ message: 'CODIGO invalido' }).end()
        return null
    }
    if (!req.params.CODIGO) {
        res.status(400).send({ message: 'CODIGO não existe' }).end()
        return null
    }

    try {
        const context = {}
        context.CODIGO = req.params.CODIGO
        const rows = await Periferico.delRegistro_db(context)
        res.status(200).json(rows)
    } catch (err) {
        res.status(500).end()
    }
}
module.exports.delRegistro = delRegistro




async function getBuscarCodigoPorTag(TAG) {
    if (validator.isEmpty(TAG + '')) {
        return 'erro';
    }
    try {
        const context = {}
        context.TAG = TAG

        const rows = await Periferico.getBuscarCodigoPorTag_db(context)
        if (rows) {
            return rows
        } else {
            return 'erro';
        }
    } catch (err) {
        return 'erro';
    }
}
module.exports.getBuscarCodigoPorTag = getBuscarCodigoPorTag







async function getAtualizaSensoresESP(req, res, next) {
 
    try {
        const context = {}
        context.COD_LOCAL = req.params.COD_LOCAL
        const rows = await Periferico.getAtualizaSensoresESP_db(context)
 
        res.status(200).json(rows)
    } catch (err) {
        res.status(500).end()
    }
}
module.exports.getAtualizaSensoresESP = getAtualizaSensoresESP